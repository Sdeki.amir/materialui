import React from 'react';
import { create } from 'jss';
import rtl from 'jss-rtl';
import { StylesProvider, jssPreset, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
const theme = createMuiTheme({
    direction: 'rtl',
});
// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

export default (props: any) => {
    return (
        <StylesProvider jss={jss}>
            <ThemeProvider theme={theme}>
                {props.children}
            </ThemeProvider>
        </StylesProvider>
    );
}