import React from "react";
import {
  Button,
  TextField,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
} from "@material-ui/core";
import MyButton from "../src/components/common/button/loginButton";
import MenuIcon from "@material-ui/icons/Menu";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  })
);

function App() {
  const classes = useStyles();
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            News
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
      <TextField title="Name" fullWidth />
      <MyButton title="ورود" iconClass="df"></MyButton>
    </div>
  );
}

export default App;
