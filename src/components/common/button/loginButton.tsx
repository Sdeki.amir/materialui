import React from "react";

import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

interface LoginButtonProps {
  title: string;
  iconClass: string;
}

const useStyles = makeStyles({
  root: {
    background:
      "linear-gradient(45deg, rgba(43, 50, 177, 1) 0%, rgba(20, 135, 203, 1) 100%)",
    border: 0,
    borderRadius: 25,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
    height: "2.5rem",
    padding: "0 30px",
    minWidth: "30vw",
    "& span": {
      "font-size": "1.4rem",
    },
  },
});

const LoginButton: React.FC<LoginButtonProps> = (props) => {
  const classes = useStyles();
  return (
    <Button className={classes.root}>
      <span>{props.title}</span>
      <span className={props.iconClass}></span>
    </Button>
  );
};

export default LoginButton;
